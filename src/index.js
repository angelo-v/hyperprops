import React from "react";
import ReactDOM from "react-dom";

import {provide} from "./lib";

const resource = provide(
    // provide a base context, used by all components
    // this is good to declare common prefixes or often used properties
    {
      'foaf': 'http://xmlns.com/foaf/0.1/'
    }
);

// pure presentational component...
const Link = ({href, label}) => <a href={href}>{label}</a>;

// ... coming to live with hyperfact
const PersonLink = resource(
    Link,
    // specify which props need to be passed to the presentational component
    ["href", "label"],
    // specify which props should be passed, but are optional
    [],
    {
      // provide a specific context to map JSON-LD properties to component specific props
      href: "@id",
      label: "foaf:name",
    }
);

const DebugDemo = resource(() => null,
    ['name'], ['knows', 'role', 'address'],
    {
      "@vocab": "http://xmlns.com/foaf/0.1/",
      "role": "http://www.w3.org/2006/vcard/ns#role"
    }, {debug: true});

const Demo = () =>
    <section>
      <h1>Hyperprops Demo</h1>
      <p>Welcome to hyperprops, by <PersonLink
          id="https://angelo.veltens.org/profile/card#me"/></p>

      <h2>Debug mode</h2>
      <DebugDemo id="https://angelo.veltens.org/profile/card#me"/>
    </section>;

ReactDOM.render(<Demo />, document.getElementById("root"));
