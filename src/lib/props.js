export function getRelevantProps (requiredProps, resource) {
  return requiredProps.reduce((object, key) => {
    const value = resource[key];
    if (value !== undefined) {
      object[key] = value;
    } else {
      object.missing.push(key);
    }
    return object
  }, {missing: []});
}
