import React from 'react';
import ldProvider from '../resource';
import {shallow} from 'enzyme';

describe('Given a linked data provider returns facts about a person and uses schema.org as base vocab', () => {

  let resource;
  let fetchResource;
  let subscribe;
  let unsubscribe;
  beforeEach(() => {
    const person = {
      '@context': {
        'schema': 'http://schema.org/',
        'ex': 'http://vocab.example/'
      },
      '@id': 'https://person.example',
      '@type': 'schema:Person',
      'schema:name': 'John Doe',
      'schema:jobTitle': 'Software Developer',
      'ex:whatever': 'Non-relevant data'
    };

    fetchResource = jest.fn();
    subscribe = jest.fn();
    unsubscribe = jest.fn();
    let baseContext = {
      '@vocab': 'http://schema.org/',
    };
    resource = ldProvider({
      get: (id) => Promise.resolve(person),
      fetch: fetchResource,
      subscribe,
      unsubscribe
    }, baseContext);
  });

  describe(
      'and a resource container maps schema.org properties to a react component',
      () => {

        let DataComponent;
        beforeEach(() => {
          const PresentationalComponent = () => null;
          DataComponent = resource(PresentationalComponent, ['name', 'jobTitle']);
        });

        describe('when the resource container is rendered with the persons ID',
            () => {

              let result;
              beforeEach(() => {
                result = shallow(<DataComponent
                    id="https://person.example"/>);
              });

              it('then the person data is present in the component props on next tick',
                  (done) => {
                    setTimeout(() => {
                      expect(result).toHaveProp('name', 'John Doe');
                      expect(result).toHaveProp('jobTitle', 'Software Developer');
                      done()
                    }, 100);
                  });

            });

      });

  describe(
      'and a resource container maps properties using an additional context',
      () => {

        let DataComponent;
        beforeEach(() => {
          const PresentationalComponent = () => null;
          const additionalContext = {
            "job": "jobTitle"
          };
          DataComponent = resource(PresentationalComponent, ['name', 'job'], [], additionalContext);
        });

        describe('when the resource container is rendered with the persons ID',
            () => {

              let result;
              beforeEach(() => {
                result = shallow(<DataComponent
                    id="https://person.example"/>);
              });

              it('then the person data is present in the component props on next tick',
                  (done) => {
                    setTimeout(() => {
                      expect(result).toHaveProp('name', 'John Doe');
                      expect(result).toHaveProp('job', 'Software Developer');
                      done()
                    }, 100);
                  });

            });

      });
});
