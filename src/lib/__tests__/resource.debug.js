import React from 'react';
import ldProvider from '../resource';
import {shallow} from 'enzyme';

import Debug from '../debug';

describe('Given a linked data provider returns facts about a person', () => {

  let resource;
  let fetchResource;
  let subscribe;
  let unsubscribe;
  beforeEach(() => {
    const person = {
      '@context': {
        'schema': 'http://schema.org/',
        'ex': 'http://vocab.example/'
      },
      '@id': 'https://person.example',
      '@type': 'schema:Person',
      'schema:name': 'John Doe',
      'schema:jobTitle': 'Software Developer',
      'ex:whatever': 'Non-relevant data'
    };

    fetchResource = jest.fn();
    subscribe = jest.fn();
    unsubscribe = jest.fn();
    resource = ldProvider({
      get: (id) => Promise.resolve(person),
      fetch: fetchResource,
      subscribe,
      unsubscribe
    });
  });

  describe(
      'and a resource container is in debug mode',
      () => {

        let DataComponent;
        let PresentationalComponent;
        beforeEach(() => {
          PresentationalComponent = () => null;
          DataComponent = resource(PresentationalComponent,
              ['name', 'missingRequiredProp'],
              ['job', 'missingOptionalProp'],
              {
                '@vocab': 'http://schema.org/',
                job: 'jobTitle'
              },
              {debug: true});
        });

        describe('when the resource container is rendered with the persons ID',
            () => {

              let result;
              beforeEach(() => {
                result = shallow(<DataComponent
                    id="https://person.example"/>);
              });

              it('then a debug component is rendered', () => {
                const debug = result.find(Debug);
                expect(debug).toExist();
                expect(debug).toHaveProp("id", "https://person.example")
              });

              it('and the debug component contains compacted resource data on next tick', (done) => {
                setTimeout(() => {
                  const debug = result.find(Debug);
                  expect(debug).toHaveProp('data', {
                    '@context': {
                      '@vocab': 'http://schema.org/',
                      'job': 'jobTitle'
                    },
                    '@id': 'https://person.example',
                    '@type': 'Person',
                    'name': 'John Doe',
                    'job': 'Software Developer',
                    'http://vocab.example/whatever': 'Non-relevant data'
                  });
                  done()
                },100);
              });

              it('and the debug component contains the simplified properties on next tick', (done) => {
                setTimeout(() => {
                  const debug = result.find(Debug);
                  expect(debug).toHaveProp('props', {
                    'name': 'John Doe',
                    'job': 'Software Developer'
                  });
                  done()
                },100);
              });

              it('and the debug component contains the missing properties on next tick', (done) => {
                setTimeout(() => {
                  const debug = result.find(Debug);
                  expect(debug).toHaveProp('missing', ['missingRequiredProp', 'missingOptionalProp']);
                  done()
                },100);
              });


            });

        describe('when the resource container is rendered without an ID',
            () => {

              let result;
              beforeEach(() => {
                result = shallow(<DataComponent />);
              });

              it('then a debug component is rendered', () => {
                const debug = result.find(Debug);
                expect(debug).toExist();
              });

              it('and the debug component contains an error', () => {
                  const debug = result.find(Debug);
                  expect(debug).toHaveProp('data', {});
                  expect(debug).toHaveProp('error', 'Resource(PresentationalComponent) needs an ID');
              });


            });
      });

});
