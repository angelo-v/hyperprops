import React from 'react';
import ldProvider from '../resource';
import {shallow} from 'enzyme';

describe('Given a linked data provider returns facts about a person', () => {

  let resource;
  let fetchResource;
  let subscribe;
  let unsubscribe;
  beforeEach(() => {
    const person = {
      '@context': {
        'schema': 'http://schema.org/',
        'ex': 'http://vocab.example/'
      },
      '@id': 'https://person.example',
      '@type': 'schema:Person',
      'schema:name': 'John Doe',
      'schema:jobTitle': 'Software Developer',
      'ex:whatever': 'Non-relevant data'
    };

    fetchResource = jest.fn();
    subscribe = jest.fn();
    unsubscribe = jest.fn();
    resource = ldProvider({
      get: (id) => Promise.resolve(person),
      fetch: fetchResource,
      subscribe,
      unsubscribe
    });
  });

  describe(
      'and a resource container maps schema.org properties to a react component',
      () => {

        let DataComponent;
        let PresentationalComponent;
        beforeEach(() => {
          PresentationalComponent = () => null;
          DataComponent = resource(PresentationalComponent,
              ['name', 'job'],
              [],
              {
            '@vocab': 'http://schema.org/',
            job: 'jobTitle'
              });
        });

        describe('when the resource container is rendered with the persons ID',
            () => {

              let result;
              beforeEach(() => {
                result = shallow(<DataComponent
                    id="https://person.example"/>);
              });

              it('then the person data is present in the component props on next tick',
                  (done) => {
                    setTimeout(() => {
                      expect(result.find(PresentationalComponent)).toExist();
                      expect(result).toHaveProp('name', 'John Doe');
                      expect(result).toHaveProp('job', 'Software Developer');
                      done()
                    }, 100);
                  });

              it('does not pass irrelevant data',
                  (done) => {
                    process.nextTick(() => {
                      expect(result).not.toHaveProp('schema');
                      expect(result).not.toHaveProp('@context');
                      expect(result).not.toHaveProp('@id');
                      expect(result).not.toHaveProp('@type');
                      expect(result).not.toHaveProp('http://vocab.example/whatever');
                      done()
                    });
                  });

              it('subscribes to updates', () => {
                expect(subscribe).toHaveBeenCalledWith('https://person.example', expect.any(Function))
              });

              describe('and unmounted afterwards', () => {
                beforeEach((done) => {
                  process.nextTick(() => {
                    result.unmount();
                    done();
                  });
                });

                it('it unsubscribes', () => {
                  expect(unsubscribe).toHaveBeenCalledWith("https://person.example", expect.any(Function))
                });

              });

            });

        describe(
            'when the resource container is rendered with additional props',
            () => {
              let result;
              beforeEach(() => {
                result = shallow(<DataComponent
                    id="https://person.example"
                    additional="text"
                    name="Fallback name"
                    job="Fallback job"
                />);
              });

              it('then the comoponent is still pending until relevant props are provided',
                  () => {
                    expect(result.find(PresentationalComponent)).not.toExist();
                    expect(result).not.toHaveProp('name');
                    expect(result).not.toHaveProp('job');
                    expect(result).not.toHaveProp('additional');
                    expect(result).toHaveText('Loading...');
                  });

              it('and provided data gets precedence when names are clashing',
                  (done) => {
                    process.nextTick(() => {
                      expect(result).toHaveProp('name', 'John Doe');
                      expect(result).toHaveProp('additional', 'text');
                      done()
                    });
                  });

            });
      });


  describe(
      'and a resource container maps has declares some optional props',
      () => {

        let DataComponent;
        let PresentationalComponent;
        beforeEach(() => {
          PresentationalComponent = () => null;
          DataComponent = resource(PresentationalComponent,
              ['name'],
              ['job', 'address'],
              {
                '@vocab': 'http://schema.org/',
                job: 'jobTitle'
              });
        });

        describe('when the resource container is rendered with the persons ID',
            () => {

              let result;
              beforeEach(() => {
                result = shallow(<DataComponent
                    id="https://person.example"/>);
              });

              it('then the person data is present in the component props on next tick',
                  (done) => {
                    setTimeout(() => {
                      expect(result.find(PresentationalComponent)).toExist();
                      expect(result).toHaveProp('name', 'John Doe');
                      expect(result).toHaveProp('job', 'Software Developer');
                      done()
                    }, 100);
                  });

            });

        describe(
            'when the resource container is rendered with additional props',
            () => {
              let result;
              beforeEach(() => {
                result = shallow(<DataComponent
                    id="https://person.example"
                    additional="text"
                    name="Fallback name"
                    job="Fallback job"
                    address="Fallback address"
                />);
              });

              it('then the comoponent is still pending until relevant props are provided',
                  () => {
                    expect(result.find(PresentationalComponent)).not.toExist();
                    expect(result).not.toHaveProp('name');
                    expect(result).not.toHaveProp('job');
                    expect(result).not.toHaveProp('additional');
                    expect(result).toHaveText('Loading...');
                  });

              it('and optional props do not get overridden',
                  (done) => {
                    process.nextTick(() => {
                      expect(result).toHaveProp('name', 'John Doe');
                      expect(result).toHaveProp('job', 'Software Developer');
                      expect(result).toHaveProp('additional', 'text');
                      expect(result).toHaveProp('address', 'Fallback address');
                      done()
                    });
                  });

            });
      });

});
