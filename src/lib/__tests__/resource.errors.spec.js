import React from 'react';
import ldProvider from '../resource';
import {shallow} from 'enzyme';

describe('Given a linked data provider returns some data', () => {

  let resource;
  let fetchResource;
  let subscribe;
  let unsubscribe;
  beforeEach(() => {
    const person = {
      '@id': 'https://resource.example',
      'http://vocab.example/error': 'Server side error',
    };

    fetchResource = jest.fn();
    subscribe = jest.fn();
    unsubscribe = jest.fn();

    resource = ldProvider({
      get: (id) => Promise.resolve(person),
      fetch: fetchResource,
      subscribe,
      unsubscribe
    });

  });

  describe(
      'and a resource container maps vocabulary properties to a prop named "error"',
      () => {

        let DataComponent;
        beforeEach(() => {
          const PresentationalComponent = () => null;
          DataComponent = resource(PresentationalComponent,
              ['error'],
              [],
              {
            error: 'http://vocab.example/error',
              });
        });

        describe('when the resource container is rendered with the resource ID',
            () => {

              let result;
              beforeEach(() => {
                result = shallow(<DataComponent
                    id="https://resource.example"/>);
              });


              it('then the "error" prop contains the provided data on next tick',
                  (done) => {
                    process.nextTick(() => {
                      expect(result).toHaveProp('error', 'Server side error');
                      done()
                    });
                  });
            });


        describe('when the resource container is rendered without ID',
            () => {

              let result;
              beforeEach(() => {
                result = shallow(<DataComponent/>);
              });

              it('then an error is rendered to indicate that',
                  (done) => {
                    process.nextTick(() => {
                      expect(result).toHaveText('Error: Resource(PresentationalComponent) needs an ID');
                      done()
                    });
                  });

              it('nothing is fetched', () => {
                expect(fetchResource).not.toHaveBeenCalled()
              });

              it('nothing is subscribed', () => {
                expect(subscribe).not.toHaveBeenCalled()
              });

              describe('and unmounted afterwards', () => {
                beforeEach(() => {
                  result.unmount()
                });

                it('it unsubscribes', () => {
                  expect(unsubscribe).not.toHaveBeenCalled()
                });

              });

            });
      });
});
