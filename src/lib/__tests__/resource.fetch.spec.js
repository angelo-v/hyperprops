import React from 'react';
import ldProvider from '../resource';
import {shallow} from 'enzyme';

describe('Given a linked data provider returns a person with a name', () => {

  let resource;
  let fetchResource;
  let updateResource = {};
  beforeEach(() => {
    const person = {
      '@context': {
        'schema': 'http://schema.org/'
      },
      '@id': 'https://person.example',
      'schema:name': 'John Doe'
    };

    fetchResource = jest.fn();
    resource = ldProvider({
      get: (id) => Promise.resolve(person),
      fetch: fetchResource,
      subscribe: (id, callback) => {
        updateResource[id] = callback;
      },
      unsubscribe: jest.fn()
    });
  });

  describe(
      'and a resource container requires only the name to render',
      () => {

        let DataComponent;
        let PresentationalComponent;
        beforeEach(() => {
          PresentationalComponent = () => null;
          DataComponent = resource(PresentationalComponent,
              ['name'],
              [],
              {'@vocab': 'http://schema.org/'}
          );
        });

        describe('when the resource container is rendered with the persons ID',
            () => {

              beforeEach(() => {
                shallow(<DataComponent id="https://person.example"/>);
              });

              it('does not fetch the person resource since the name is already present',
                  (done) => {
                    process.nextTick(() => {
                      expect(fetchResource).not.toHaveBeenCalled()
                      done()
                    })
                  });

            });

      });

  describe(
      'and a resource container requires the name and the jobTitle to render',
      () => {

        let DataComponent;
        let PresentationalComponent;
        beforeEach(() => {
          PresentationalComponent = () => null;
          DataComponent = resource(PresentationalComponent,
              ['name', 'jobTitle'],
              [],
              {'@vocab': 'http://schema.org/'}
          );
        });

        describe('when the resource container is rendered with the persons ID',
            () => {

              beforeEach(() => {
                shallow(<DataComponent id="https://person.example"/>);
              });

              it('does fetch the person resource since the jobTitle is missing',
                  (done) => {
                    process.nextTick(() => {
                      expect(fetchResource).toHaveBeenCalledTimes(1);
                      expect(fetchResource).toHaveBeenCalledWith(
                          "https://person.example");
                      done();
                    })
                  });

              describe('and it is updated but jobTitle is still missing', () => {

                beforeEach(async () => {
                  await updateResource["https://person.example"]({
                    '@id': 'https://person.example',
                    'http://schema.org/name': 'John Doe',
                  });
                });

                it('then it still fetched only once', () => {
                  expect(fetchResource).toHaveBeenCalledTimes(1);
                  expect(fetchResource).toHaveBeenCalledWith(
                      "https://person.example");
                });

              });

            });

      });

});
