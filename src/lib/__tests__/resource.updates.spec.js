import React from 'react';
import ldProvider from '../resource';
import {shallow} from 'enzyme';

describe('Given a linked data provider that notifies subscribers on update', () => {

  let resource;
  let updateResource = {};
  beforeEach(() => {

    resource = ldProvider({
      get: () => null,
      fetch: () => null,
      subscribe: (id, callback) => {
        updateResource[id] = callback;
      },
      unsubscribe: () => null
    });
  });

  describe(
      'and a resource container maps schema.org properties to a react component',
      () => {

        let DataComponent;
        let PresentationalComponent;
        beforeEach(() => {
          PresentationalComponent = () => null;
          DataComponent = resource(PresentationalComponent,
              ['name', 'job'], [], {
            name: 'http://schema.org/name',
            job: 'http://schema.org/jobTitle'
              });
        });

        describe('when the resource container is rendered with the persons ID',
            () => {

              let result;
              beforeEach(() => {
                result = shallow(<DataComponent
                    id="https://person.example"/>);
              });

              it('then the person data is not yet present, instead it indicates loading',
                  (done) => {
                    process.nextTick(() => {
                      expect(result.find(PresentationalComponent)).not.toExist();
                      expect(result).not.toHaveProp('name');
                      expect(result).not.toHaveProp('job');
                      expect(result).toHaveText('Loading...');
                      done()
                    });
                  });

              describe('when person data is updated', () => {

                beforeEach(async () => {
                    await updateResource["https://person.example"]({
                      '@id': 'https://person.example',
                      'http://schema.org/name': 'John Doe',
                      'http://schema.org/jobTitle': 'Software Developer'
                    });
                });

                it('then the person data is present in the component props on next tick',
                    (done) => {
                      process.nextTick(() => {
                        expect(result.find(PresentationalComponent)).toExist();
                        expect(result).toHaveProp('name', 'John Doe');
                        expect(result).toHaveProp('job', 'Software Developer');
                        done()
                      });
                    });

              });

            });
      });
});
