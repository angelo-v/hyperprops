import React from "react";

import ReactJson from 'react-json-view'

import styles from './debug.module.css'


export default ({id, data, error, missing, props}) => <>
  {error !== undefined ? <span className={styles.error}>{error}</span> : null }
  <ReactJson
    collapsed
    sortKeys
    name={id}
    src={data}
    />
  <ReactJson
      collapsed
      sortKeys
      name="properties"
      src={props}
  />
  <ReactJson
      sortKeys
      name="missing properties"
      src={missing}
  />
</>
