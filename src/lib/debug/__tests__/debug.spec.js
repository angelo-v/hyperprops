import React from 'react';
import {shallow} from 'enzyme';

import Debug from '../';
import ReactJson from 'react-json-view'


describe('When a debug component is rendered', function () {

  let result;
  let dataTree;
  let propertiesTree;
  let missingPropertiesTree;

  beforeEach(() => {
    result = shallow(<Debug
        id="https://resource.example/"
        data={{"foo": 42}}
        props={{"bar": "value"}}
        missing={['one', 'two']}
    />);
    dataTree = result.find({ name: "https://resource.example/" });
    propertiesTree = result.find({ name: "properties" });
    missingPropertiesTree = result.find({ name: "missing properties" });
  });

  it('then it shows a collapsed, ordered JSON tree for the resource data', () => {
    expect(dataTree).toExist();
    expect(dataTree).toMatchSelector(ReactJson);
    expect(dataTree).toHaveProp('collapsed', true);
    expect(dataTree).toHaveProp('sortKeys', true);
  });

  it('and the data tree shows the resource data', () => {
    expect(dataTree).toHaveProp('src', {"foo": 42})
  });

  it('and it shows a collapsed, ordered JSON tree for the properties', () => {
    expect(propertiesTree).toExist();
    expect(propertiesTree).toMatchSelector(ReactJson);
    expect(propertiesTree).toHaveProp('collapsed', true);
    expect(propertiesTree).toHaveProp('sortKeys', true);
  });

  it('and the properties tree shows the props', () => {
    expect(propertiesTree).toHaveProp('src', {"bar": "value"})
  });

  it('and it shows a non-collapsed, ordered JSON tree for missing properties', () => {
    expect(missingPropertiesTree).toExist();
    expect(missingPropertiesTree).toMatchSelector(ReactJson);
    expect(missingPropertiesTree).toHaveProp('collapsed', false);
    expect(missingPropertiesTree).toHaveProp('sortKeys', true);
  });

  it('and the missing properties are shown', () => {
    expect(missingPropertiesTree).toHaveProp('src', ["one", "two"])
  });

  it('no error message is shown', () => {
    let error = result.find('.error');
    expect(error).not.toExist();
  });

});

describe('When a debug component is rendered with an error', function () {

  let result;

  beforeEach(() => {
    result = shallow(<Debug id="https://resource.example/"
                            resource={{"foo": 42}}
                            error="Something went wrong"/>);
  });

  it('then an error message is shown', () => {
    let error = result.find('.error');
    expect(error).toExist();
    expect(error).toHaveText("Something went wrong")
  });

});
