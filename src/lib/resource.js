import React, {Component} from 'react'

import simplifyWith from './simplify';

import Debug from './debug';

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName ||
      WrappedComponent.name || 'Component'
}

export default (provider, baseContext = {}) => (WrappedComponent, requiredProps = [], optionalProps = [], context = {}, options = {debug: false}) => {

    const simplify = simplifyWith([baseContext, context], requiredProps, optionalProps);

    return class ResourceContainer extends Component {

      static displayName = `Resource(${getDisplayName(WrappedComponent)})`;

      state = {
        data: {},
        error: undefined,
        fetched: false
      };

      applyContext = async (data) => {
        const result = await simplify(data);
        if (!result.complete && !this.state.fetched) {
          provider.fetch(this.props.id);
          this.setState({
            fetched: true
          });
        }
        this.setState(result);
      };

      async componentDidMount() {
        const { id } = this.props;
        if (!id) {
          this.setState({error: `${ResourceContainer.displayName} needs an ID`});
          return
        }
        provider.subscribe(id, this.applyContext);
        const data = await provider.get(id);
        if (data) {
          await this.applyContext(data);
        }
      }

      componentWillUnmount() {
        if (this.props.id) {
          provider.unsubscribe(this.props.id, this.applyContext);
        }
      }

      render() {

        if (options.debug) {
          return this.renderDebug()
        }

        if (this.state.error) {
          return `Error: ${this.state.error}`
        }
        if (!this.state.ready) {
          return 'Loading...'
        }
        return <WrappedComponent {...this.props}
                                 {...this.state.props} />
      }

      renderDebug() {
        return <Debug
            id={this.props.id}
            data={this.state.data}
            props={this.state.props}
            missing={this.state.missing}
            error={this.state.error}/>
      }
    }

}
