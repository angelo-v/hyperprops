import provideResource from './resource';

import hyperfact from './provider-hyperfact';

const provide = (context, provider = hyperfact) => provideResource(provider, context);

export { provide }

