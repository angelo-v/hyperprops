import simplifyWith from '../'

describe('Given simplify requires a name prop', () => {

  const simplify = simplifyWith({
    "name": "http://schema.org/name"
  }, ["name"]);

  describe('when then simplified resource contains a name', () => {

    let result;
    beforeEach(async () => {
      result = await simplify({
        "http://schema.org/name": "Jane Doe"
      });
    });

    it('then the result props contain the name', () => {
      expect(result.props).toEqual({
        "name": "Jane Doe"
      })
    });

    it('then the result lists nothing as missing', () => {
      expect(result.missing).toEqual([])
    });

    it('then the result is complete', () => {
      expect(result.complete).toBeTruthy()
    });

    it('then the result is ready', () => {
      expect(result.ready).toBeTruthy()
    });

  });

  describe('when then simplified resource does not contain a name', () => {

    let result;
    beforeEach(async () => {
      result = await simplify({
        "http://schema.org/jobTitle": "Software Developer"
      });
    });

    it('then the result lists name as missing', () => {
      expect(result.missing).toEqual(["name"])
    });

    it('then the result is incomplete', () => {
      expect(result.complete).toBeFalsy()
    });

    it('then the result is not ready', () => {
      expect(result.ready).toBeFalsy();
    });

  });

});
