import simplifyWith from '../'

describe('Given a context is used to simplify', () => {

  const simplify = simplifyWith({
    "name": "http://schema.org/name"
  });

  describe('when resource data is simplified', () => {

    let result;
    beforeEach(async () => {
      result = await simplify({
        "http://schema.org/name": "Jane Doe"
      });
    });

    it('then the result data is compacted JSON-LD', () => {
      expect(result.data).toEqual({
        "@context": {
          "name": "http://schema.org/name",
        },
        "name": "Jane Doe"
      })
    });

  });

});
