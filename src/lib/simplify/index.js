import {compact} from "jsonld";

import {getRelevantProps} from '../props';

export default (context, requiredProps = [], optionalProps = []) => async (resource) => {
  const data = await compact(resource, context);
  const { missing: missingRequired, ...relevantProps} = getRelevantProps(requiredProps, data);
  const { missing: missingOptional, ...moreProps} = getRelevantProps(optionalProps, data);
  return {
    data,
    missing: [
        ...missingRequired,
        ...missingOptional
    ],
    complete: missingRequired.length === 0 && missingOptional.length === 0,
    ready: missingRequired.length === 0,
    props: {
      ...relevantProps,
      ...moreProps
    }
  }
};
