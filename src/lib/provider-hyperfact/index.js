import createStore from 'hyperfact';
import hyperfetch from 'hyperfetch';

export const facts = createStore();

export default {
  get: (id) => facts.getResource(id),
  fetch: (id) => hyperfetch(id).then(resource => facts.merge(resource)),
  subscribe: (id, handler) => facts.subscribe(id, handler),
  unsubscribe: (id, handler) => facts.unsubscribe(id, handler)
};
