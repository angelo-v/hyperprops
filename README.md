# Hyperprops

Connect the web of linked data to presentational react components.

```javascript

import {provide} from "hyperprops";

const resource = provide(
    // provide a base context, used by all components
    // this is good to declare common prefixes or often used properties
    {
      'foaf': 'http://xmlns.com/foaf/0.1/'
    }
);

// pure presentational component...
const Link = ({href, label}) => <a href={href}>{label}</a>;

// ... coming to live with hyperfact
const PersonLink = resource(
    Link,
    // specify which props need to be passed to the presentational component
    ["href", "label"],
    // specify which props should be passed, but are optional
    [],
    {
      // provide a specific context to map JSON-LD properties to component specific props
      href: "@id",
      label: "foaf:name",
    }
);


```

Usage:

```JSX
<PersonLink id="https://angelo.veltens.org/profile/card#me"/>
```

## Debug mode

```javascript
const PersonLink = resource(
    Link,
    ["href", "label"], [],
    {
      href: "@id",
      label: "foaf:name",
    },
    { debug: true }
);
```
