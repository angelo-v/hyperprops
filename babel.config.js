module.exports = {
  presets: [
    "@babel/preset-env",
    [
      'react-app',
      {"absoluteRuntime": false, "useESModules": false}
    ],
  ],
  "plugins": ["css-modules-transform"]
};
